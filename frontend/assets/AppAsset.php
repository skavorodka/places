<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/design/css/reset.css',
        '/design/css/plugins.css',
        '/design/css/style.css',
        '/design/css/color.css',
        'css/site.css',
    ];
    public $js = [
        //'//api-maps.yandex.ru/2.1/?lang=ru_RU',
        '/design/js/plugins.js',
        '/design/js/scripts.js',
        '//maps.googleapis.com/maps/api/js?key=AIzaSyDQKUmK68zVFLfmLgpVtrd3cflS3PbCt5c&libraries=places&callback=initAutocomplete',
        '/design/js/map-plugins.js',
        '/design/js/map-listing.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
